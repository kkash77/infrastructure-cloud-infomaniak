%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate


<br>

openvpn

    server ( CA + cert + network )  >  client ( cert + interface + routes )


-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate


<br>

Create new variables : client list and local key destination

```
vpn_user_list:
  - xpestel
destination_key: "/tmp/"
```


-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate


<br>

Create a block of tasks iterated by client

```
- name: "Create client - {{ item.name }}"
  include_tasks: create_client.yml
  with_items: "{{ vpn_user_list }}"
  tags:
  - fetch
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate


<br>

Check if client certificate already exists

```
- name: "{{ item }} - client certificate key already exists"
  stat:
    path: "{{ ansible_env.HOME }}/openvpn-ca/pki/reqs/{{ item }}.req"
  register: __check__certificate_key
```

<br>

If not generate client certificate

```
- name: "{{ item }} - Generate client certificate key"
  shell: ./easyrsa --batch build-client-full {{ item }} nopass
  args: 
    chdir: "{{ ansible_env.HOME }}/openvpn-ca/"
    executable: /bin/bash
  when: __check__certificate_key.stat.exists == false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate

<br>

Create a dedicated directory for the client

```
- name: "{{ item }} - Create client certificate configs dir"
  file: 
    path: "{{ ansible_env.HOME }}/openvpn-ca/{{ item }}"
    state: directory
    mode: 0750
```

<br>

Then copy cert into it

```
- name: "{{ item }} - Copy client sample configs from remote host itself"
  copy:
    remote_src: yes
    src: /usr/share/doc/openvpn/examples/sample-config-files/client.conf
    dest: "{{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate

<br>

Create ovpn file for the client

```
- name: "{{ item }} - Set the server ip and port"
  lineinfile:
    dest: "{{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn"
    regexp: "^{{ lines.regex | regex_escape() }}"
    line: "{{ lines.value }}"
  loop:
    - { regex: 'remote my-server-1 1194', value: 'remote {{ ansible_host }} 1194' }
    - { regex: ';user nobody', value: 'user nobody' }
    - { regex: ';group nogroup', value: 'group nogroup' }
    - { regex: 'ca ca.crt', value: '#ca ca.crt' }
    - { regex: 'cert client.crt', value: '#cert client.crt' }
    - { regex: 'key client.key', value: '#key client.key' }
    - { regex: 'tls-auth ta.key 1', value: '#tls-auth ta.key 1' }
    - { regex: 'cipher AES-256-CBC', value: 'cipher AES-128-GCM' }
  loop_control:
    loop_var: lines
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate

<br>

Add some secure lines

```
- name: "{{ item }} - Add some secure lines"
  lineinfile:
    dest: "{{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn"
    line: "{{ lines }}"
  loop:
    - "comp-lzo"
    - "auth SHA256"
    - "tls-client"
    - "tls-version-min 1.2"
    - "tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256"
    - "ignore-unknown-option block-outside-dns"
    - "setenv opt block-outside-dns"
    - "verify-x509-name server_{{ vpn_server_name }} name"
    - "dh none"
    - "verb 3"
  loop_control:
    loop_var: lines
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate

<br>

Add certificate and key into the ovpn file

```
- name: "{{ item }} - Create client ovpn file"
  shell: "{{ item }}"
  loop:
    - echo -e '<ca>' >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - cat {{ ansible_env.HOME }}/openvpn-ca/pki/ca.crt >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - echo -e '</ca>\n<cert>' >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - cat {{ ansible_env.HOME }}/openvpn-ca/pki/issued/{{ item }}.crt >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - echo -e '</cert>\n<key>' >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - cat {{ ansible_env.HOME }}/openvpn-ca/pki/private/{{ item }}.key >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - echo -e '</key>\n<tls-auth>' >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - cat {{ ansible_env.HOME }}/openvpn-ca/pki/ta.key >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - echo -e '</tls-auth>' >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
    - echo -e 'key-direction 1' >> {{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn
  args:
    chdir: "{{ ansible_env.HOME }}/openvpn-ca/"
    executable: /bin/bash


-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn  create a client certificate

<br>

Finally, fetch the ovpn file locally

```
- name: "{{ item }} - Fetch client configurations"
  fetch:
    src: "{{ ansible_env.HOME }}/openvpn-ca/{{ item }}/{{ item }}.ovpn"
    dest: "{{ destination_key }}/"
    flat: yes
  tags:
    - fetch
```