%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Openvpn - add ansible code in terraform

Mode details : Terraform Playlist

<br>

Define an ansible.cfg file

```
[defaults]
#callback_whitelist = profile_tasks
host_key_checking = False
forks = 40
allow_world_readable_tmpfiles = true
#gathering = smart
[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o PreferredAuthentications=publickey -o ForwardAgent=yes
pipelining = true
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Openvpn - add ansible code in terraform


<br>

Split ansible playbook in 2 files

<br>

Change the loop for single entry

<br>

Create new terraform variable

```
variable "vpn_user_list" {
  type    = list(any)
  default = ["xpestel"]
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Openvpn - add ansible code in terraform

Mode details : Terraform Playlist

<br>

Create a null resource to execute shell code

```
resource "null_resource" "openvpn_server" {
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = <<-EOT
      echo > /tmp/openvpn.ini;
      echo "[openvpn]" | tee -a /tmp/openvpn.ini;
      echo "openvpn ansible_host=${openstack_networking_floatingip_v2.floatip_1.address}" | tee -a /tmp/openvpn.ini;
      ANSIBLE_CONFIG=../ansible/ansible.cfg ansible-playbook -u debian -i /tmp/openvpn.ini --private-key ~/.ssh/info ../ansible/openvpn_server.yml;
      rm -f /tmp/openvpn.ini;
    EOT
  }
    depends_on = [openstack_compute_instance_v2.openvpn]
}
```


-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Openvpn - add ansible code in terraform

Mode details : Terraform Playlist

<br>


Create another null_resource to create vpn client with each loop

```
resource "null_resource" "create_new_vpn_client" {
  for_each = toset(var.vpn_user_list)
    triggers = {
      name = each.value
      always_run = timestamp()
    }
  
  provisioner "local-exec" {
    command = <<-EOT
      echo > /tmp/openvpn.ini;
      echo "[openvpn]" | tee -a /tmp/openvpn.ini;
      echo "openvpn ansible_host=${openstack_networking_floatingip_v2.floatip_1.address}" | tee -a /tmp/openvpn.ini;
      ANSIBLE_CONFIG=../ansible/ansible.cfg ansible-playbook -u debian -i /tmp/openvpn.ini --private-key ~/.ssh/info -e vpn_user_list=${each.value} ../ansible/openvpn_client.yml;
      rm -f /tmp/openvpn.ini;
    EOT
  }
  depends_on = [openstack_compute_instance_v2.openvpn,null_resource.openvpn_server]
}
```

